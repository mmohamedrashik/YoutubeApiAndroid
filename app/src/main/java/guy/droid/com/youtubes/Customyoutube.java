package guy.droid.com.youtubes;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import it.sephiroth.android.library.picasso.Picasso;

/**
 * Created by admin on 5/23/2016.
 */
public class Customyoutube extends ArrayAdapter<String> {
    private final  YouTubeBaseActivity context;
    private final ArrayList<String> content;
    String url = "";
   public Customyoutube(YouTubeBaseActivity context,ArrayList<String> content)
   {
       super(context,R.layout.activity_ytube_list,content);
       this.context = context;
       this.content = content;
   }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.ylist_single, null, true);
      //  YouTubeThumbnailView youTubeThumbnailView = (YouTubeThumbnailView)rowView.findViewById(R.id.youtubethumb);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.imgsrc);
        url = content.get(position);
        url = extractYTId(url);
        Picasso.with(getContext()).load("http://img.youtube.com/vi/"+url+"/mqdefault.jpg").into(imageView);

     /*   youTubeThumbnailView.initialize("AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                youTubeThumbnailLoader.setVideo(url);
                youTubeThumbnailView.setActivated(true);
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });*/




        return rowView;

    }
    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()){
            vId = matcher.group(1);
        }

        else{

            Log.d("error","error");
        }
        return vId;
    }
}
