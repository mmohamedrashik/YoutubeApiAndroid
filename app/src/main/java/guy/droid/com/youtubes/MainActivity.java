package guy.droid.com.youtubes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

public class MainActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {
YouTubePlayerView youTubePlayerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtubes);

       // youTubePlayerView.initialize("AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", this);

    }



    @Override
    protected void onStart() {
        super.onStart();
        youTubePlayerView.initialize("AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", this);

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
         // youTubePlayer.loadVideo("RIe9gSRBQTk");
       // youTubePlayer.cuePlaylist("RIe9gSRBQTk");
        youTubePlayer.cueVideo("RIe9gSRBQTk");

        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
    public void OnClick(View view)
    {
        Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, "AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", "RIe9gSRBQTk");
        startActivity(intent);
    }
}
