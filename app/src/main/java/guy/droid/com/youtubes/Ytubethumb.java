package guy.droid.com.youtubes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

public class Ytubethumb extends AppCompatActivity {
YouTubeThumbnailView youTubeThumbnailView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ytubethumb);
        youTubeThumbnailView = (YouTubeThumbnailView)findViewById(R.id.youtubethumb);
        youTubeThumbnailView.initialize("AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", new YouTubeThumbnailView.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                youTubeThumbnailLoader.setVideo("RIe9gSRBQTk");
                youTubeThumbnailView.setActivated(true);
            }

            @Override
            public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

            }
        });

    }
}
