package guy.droid.com.youtubes;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeStandalonePlayer;

import java.util.ArrayList;
import java.util.concurrent.RunnableFuture;

public class YtubeList extends YouTubeBaseActivity implements ListView.OnItemClickListener
{
    ListView listView;
    Context context;
    ArrayList<String> urls,vurls;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ytube_list);
        listView = (ListView)findViewById(R.id.ygrouplist);
        urls = new ArrayList<>();
        vurls = new ArrayList<>();
        vurls.add("https://www.youtube.com/embed/OvH46eOn_kM");

                vurls.add("https://www.youtube.com/embed/RIe9gSRBQTk");
        vurls.add("https://www.youtube.com/embed/rlasf0cUfzU");

        urls.add("OvH46eOn_kM");
        urls.add("RIe9gSRBQTk");
        urls.add("rlasf0cUfzU");



  YtubeList.this.runOnUiThread(new Runnable() {
      @Override
      public void run() {
          Customyoutube adapter = new Customyoutube(YtubeList.this, vurls);
          listView.setAdapter(adapter);
          listView.setOnItemClickListener(YtubeList.this);
      }
  });





    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = YouTubeStandalonePlayer.createVideoIntent(this, "AIzaSyAK-HGnTsaqt_1BrgXTZlAH-oWo7ggMWj4", urls.get(position));
        startActivity(intent);
    }
}
